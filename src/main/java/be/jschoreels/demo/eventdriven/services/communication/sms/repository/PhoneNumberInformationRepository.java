package be.jschoreels.demo.eventdriven.services.communication.sms.repository;


import be.jschoreels.demo.eventdriven.services.communication.sms.domain.PhoneNumberInformation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneNumberInformationRepository extends CrudRepository<PhoneNumberInformation, String> {
}
