package be.jschoreels.demo.eventdriven.services.communication.sms.consumer;

import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertTriggered;
import be.jschoreels.demo.eventdriven.services.communicate.api.domain.Message;
import be.jschoreels.demo.eventdriven.services.communicate.api.event.MessageSent;
import be.jschoreels.demo.eventdriven.services.communication.sms.domain.PhoneNumberInformation;
import be.jschoreels.demo.eventdriven.services.communication.sms.repository.PhoneNumberInformationRepository;
import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceDecreased;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountCreated;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountDeleted;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.function.Function;

@Component
public class Receiver {

    public static Logger logger = LoggerFactory.getLogger(Receiver.class);

    private final JmsTemplate jmsTemplate;

    private final PhoneNumberInformationRepository phoneNumberInformationRepository;

    private Map<Class, Function<Object, String>> messagesPerAlertRootEvent = ImmutableMap.of(
            ItemPriceDecreased.class,
            (Object event) -> {
                ItemPriceDecreased itemPriceDecreased = new ObjectMapper().convertValue(event, ItemPriceDecreased.class);
                return String.format("The item %s has dropped below your price alert. The current price is %s",
                        itemPriceDecreased.getItem(), itemPriceDecreased.getNewPrice());
            }
    );

    @Autowired
    public Receiver(JmsTemplate jmsTemplate, PhoneNumberInformationRepository phoneNumberInformationRepository) {
        this.jmsTemplate = jmsTemplate;
        this.phoneNumberInformationRepository = phoneNumberInformationRepository;
    }


    @JmsListener(destination = "Consumer.CommunicationSms:MessageSent.VirtualTopic.Alert:AlertTriggered", containerFactory = "queueListenerFactory")
    public void receiveMessageItemPriceDecreased(AlertTriggered alertTriggered) {
        logger.info("Received <" + alertTriggered.toString() + ">");
        phoneNumberInformationRepository.findById(alertTriggered.getUser())
                .ifPresent(phoneNumberInformationRepository -> {
                    String messageToSent = messagesPerAlertRootEvent.get(
                            alertTriggered.getRootEventType()).apply(alertTriggered.getRootEventPayload());
                    logger.info("Sending sms to user {} at sms {} with body : {}",
                            phoneNumberInformationRepository.getName(),
                            phoneNumberInformationRepository.getPhoneNumber(),
                            messageToSent);
                    jmsTemplate.convertAndSend("VirtualTopic.Communication:MessageSent", MessageSent.newBuilder()
                            .withMessage(Message.newBuilder()
                                    .withBody(messageToSent)
                                    .withMessageType("sms")
                                    .build())
                            .withUser(phoneNumberInformationRepository.getName())
                            .withSentTime(ZonedDateTime.now())
                            .build());
                });
    }

    @JmsListener(destination = "Consumer.CommunicationSms.VirtualTopic.User:AccountCreated",
            containerFactory = "queueListenerFactory")
    public void receiveUserCreated(AccountCreated accountCreated) {
        String phoneNumber = accountCreated.getPhoneNumber();
        String username = accountCreated.getUsername();
        phoneNumberInformationRepository.save(
                phoneNumberInformationRepository.findById(username)
                        .orElseGet(() -> new PhoneNumberInformation(username, phoneNumber))
        );
        logger.info("Stored phone number {} for user {}", phoneNumber, username);
    }

    @JmsListener(destination = "Consumer.CommunicationSms.VirtualTopic.User:AccountDeleted",
            containerFactory = "queueListenerFactory")
    public void receiveUserDeleted(AccountDeleted accountDeleted) {
        try {
            phoneNumberInformationRepository.deleteById(accountDeleted.getUsername());
            logger.info("Deleted stored phone number of user {}", accountDeleted.getUsername());
        } catch (EmptyResultDataAccessException e) {
            logger.info("No phone number was stored for user {}", accountDeleted.getUsername());
        }

    }
}