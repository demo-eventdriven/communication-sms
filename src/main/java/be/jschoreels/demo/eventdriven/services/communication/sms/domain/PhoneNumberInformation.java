package be.jschoreels.demo.eventdriven.services.communication.sms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("PhoneNumberInformation")
public class PhoneNumberInformation {

    @Id
    private String name;

    private String phoneNumber;

    public PhoneNumberInformation(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
